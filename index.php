<?php 
	$title = "Real School";
	include 'includes/header.php'; 
?>

<section class="home-p1">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 p1">
				<ul class="list-inline">
					<li><a href="#">Home</a></li>
					<li><a href="#">Goals</a></li>
					<li>Explorer</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-sm-12 p2">
				<h3>Trip to the Palace of Versailles</h3>
				<ul class="list-inline">
					<li>ONLINE MUSEUM</li>
					<li>8+ Years</li>
				</ul>
				<img src="images/img1.png" class="img-responsive">
			</div>
			<div class="col-md-6 col-sm-12 home-p2">
				<div class="row">
					<a href="#" class="p2-btn"><span class="ti-thumb-up"></span>125<br><span class="rec">RECOMMEND</span></a>
				</div>
				<div class="row">
					<div class="home-p21">
						<h5>Live On</h5>
						<p>29 June 2020 <span>for 60 min</span></p>
						<h5>Choose Slot</h5>
						<ul class="list-inline">
							<li><a href="#">10.30 AM</a><span>2/5 Seats Left!</span></li>
							<li><a href="#">5 PM</a></li>
							<li><a href="#">6.30 PM</a><span>1/5 Seats Left!</span></li>
						</ul>
						<h5>Class Fee</h5>
						<a href="#" class="cfee"><del>INR 120</del>INR 60</a>
						<ul class="p21-l">
							<li><a href="#" class="table">Book This Class For Free</a></li>
							<li><a href="#" class="shr">Share</a></li>
						</ul>
					</div>
				</div>				
			</div>
		</div>
	</div>
</section>


<section class="part2">
	<div class="container">
		<div class="row">
			<hr style="width:36px;text-align:left;margin-left:0;border-top: 6px solid #bffe8a;border-radius: 6px;margin-bottom: 30px;">
			<h3>What Goals, Badges & Skills will be achieved in this Class?</h3>
		</div>
		<div class="row">
			<hr style="width:36px;text-align:left;margin-left:0;border-top: 6px solid #bffe8a;border-radius: 6px;margin-bottom: 30px;">
			<h3>What will be covered in this Class?</h3>
			<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exercitation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit.</p>
		</div>
		<div class="row">
			<hr style="width:36px;text-align:left;margin-left:0;border-top: 6px solid #bffe8a;border-radius: 6px;margin-bottom: 30px;">
			<h3>What are the Post-Class Assignments?</h3>
		</div>
		<div class="row">
			<hr style="width:36px;text-align:left;margin-left:0;border-top: 6px solid #bffe8a;border-radius: 6px;margin-bottom: 30px;">
			<h3>How to get ready for this class?</h3>
			<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exercitation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit.</p>
		</div>
		
		<div class="row">
			<hr style="width:36px;text-align:left;margin-left:0;border-top: 6px solid #bffe8a;border-radius: 6px;margin-bottom: 30px;">
			<h3>How to attend this class?</h3>
		</div>
	</div>
</section>


<section class="part3">
	<div class="container-fluid">
		<h2>See what our students have created</h2>
	</div>
</section>













<?php include 'includes/footer.php'; ?>