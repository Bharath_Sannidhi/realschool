<?php
	$email = 'info@ufaber.com';
	$phone = '+91 8080555766';
?>

<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title; ?></title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="shortcut icon" href="images/logo-icon.png" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="css/realschool.min.css">
	<link rel="stylesheet" type="text/css" href="css/themify-icons.min.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">	
	<link rel="stylesheet" type="text/css" href="css/fonts/Grandstander-clean.ttf">
	<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>	
	
</head>
<body>



<header>
<div class="container">
	<div class="row">
		<div class="toggle t1">
			<a class="button side-hb btn-open" href="javascript:void(0)" id="side-ul">
				<span class="a"></span>
				<span class="b"></span>
				<span class="c"></span>
			</a>
		</div>
		<div class="col-md-5 col-sm-3 col-xs-5">
			<div class="logo_block">
				<a href="index.php">
					<img src="images/logo.png" class="img-responsive" alt="Digi Resourcing" />
				</a>
			</div>
		</div>
		<?php $pagename = basename($_SERVER['PHP_SELF']);    
			if($pagename == 'about.php'){
				$aclass= 'class="active"';
			}     
			else if($pagename == 'service.php'){
				$bclass= 'class="active"';  
			}  
			else if($pagename == 'courses.php'){
				$cclass= 'class="active"';  
			}
			else if($pagename == 'certifications.php'){
				$dclass= 'class="active"';  
			}
		?>
		<div class="col-md-7 col-sm-9 col-xs-7 menu_block">

			<ul class="list-unstyled list-inline menu_list">
				<li <?php echo @$aclass; ?>>
					<a href="#">Masterclass</a>
				</li>
				<li <?php echo @$bclass; ?>>
					<a href="#">Programs</a>
				</li>
				<li <?php echo @$cclass; ?>>
					<a href="#">Challenges</a>
				</li>
				<li <?php echo @$dclass; ?>>
					<a href="#">Library</a>
				</li>
			</ul>
			
			<div class="req_demo">
				<a href="" type="button" data-toggle="modal" data-target="" ><span class="ti-user"></span>Inr 200</a>
			</div>			
		</div>
		<div class="toggle t2">
			<a class="button side-hb btn-open" href="javascript:void(0)" id="side-ul">
				<span class="a"></span>
				<span class="b"></span>
				<span class="c"></span>
			</a>
		</div>
	</div>
</div>
<div class="overlay">
	<div class="menu-block animated bounceInUp">
		<ul class="list-unstyled list-inline">
			<li class=""><a href="index.php">Masterclass</a></li>
			<li class=""><a href="about.php">Programs</a></li>
			<li class=""><a href="service.php">Challenges</a></li>
			<li class=""><a href="courses.php">Library</a></li>
			<li><a href="" type="button" data-toggle="modal" data-target="" ><span class="ti-user"></span> Inr 200</a></li>
		</ul>
	</div>
</div>
</header>

