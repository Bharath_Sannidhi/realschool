<footer>
  <div class="container">
    <div class="row section_1">
      <div class="col-md-4 col-sm-6 slideInUp wow block_1" style="animation-delay: 0.2s;">
        <img src="images/footer-logo.png" class="img-responsive">
        <p>A Product of uFaber Edutech</p>
        <ul class="list-unstyled">
          <li class=""><img src="images/pointer.png"><span> FF A-006, Art Guild House, Phoenix Market City, Kurla, Mumbai - 400070</span></li>
          <li class=""><img src="images/phn.png">+91 8080555766</li>
          <li class=""><img src="images/mail.png">info@ufaber.com</li>
        </ul>
      </div>

      <div class="col-md-2 col-sm-6 slideInUp wow block_2" style="animation-delay: 0.4s;">
        <h3>Quick Links </h3>
        <ul class="list-unstyled">
          <li><a href="#">Teach With Us</a></li>
          <li><a href="#">Refer A Friend</a></li>
          <li><a href="#">Become An Affiliate</a></li>
        </ul>
      </div>

      <div class="col-md-2 col-sm-6 slideInUp wow block_3" style="animation-delay: 0.6s;">
        <ul class="list-unstyled social_block">
          <li><a href="#">About Us</a></li>
          <li><a href="#">FAQS</a></li>
          <li><a href="#">Blog</a></li>
          <li><a href="#">Privacy Policy</a></li>
        </ul>
      </div>

      <div class="col-md-4 col-sm-6 slideInUp wow block_4" style="animation-delay: 0.8s;">
        <ul class="list-unstyled list-inline">
          <li><a href="#"><span class="ti-facebook"></span></a></li>
          <li><a href="#"><span class="ti-instagram"></span></a></li>
          <li><a href="#"><span class="ti-linkedin"></span></a></li>
        </ul>
      </div>
    </div>
  </div>
    <div class="section_2 slideInUp wow" style="animation-delay: 0.2s;">
        <p>&copy; 2019 Ufaber, All rights reserved uFaber Edutech</p>
    </div>
  </div>
</footer>



<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>




</body>
</html>
